package com.transport.dialog;


import com.transport.Main;
import com.transport.model.Token;
import com.transport.model.User;
import com.transport.view.Master;
import com.transport.view.RequestApi;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DialogConnexionController {
	@FXML
	private TextField txtUsername;
	@FXML
	private TextField txtPassword;
	@FXML
	private DialogPane dpNetwork;

	@FXML
	private Button btConnexion;

	private Stage dialogStage;
	private boolean okClicked = false;

	private Main main;

	@FXML
	public void action(){
		if(isInputValid()){
			User user = new User(txtUsername.getText(), txtPassword.getText());
			System.out.println(user);
			RequestApi.convertFromUserToJson(user, "/api/auth/login", 1);
			Token token = RequestApi.convertFromJsonToToken();
			if(token != null){
				Master.setToken(token);
				System.out.println(Master.getToken().getToken());
			}else{
				/*
				 * Message erreur dans le login
				 */
			}
		}
		main.showOverview();
		okClicked = true;
		dialogStage.close();
	}

	private boolean isInputValid() {
        String errorMessage = "";

        if (txtUsername.getText() == null || txtUsername.getText().length() == 0) {
            errorMessage += "Le champ Username est vide!\n";
        }

        if (txtPassword.getText() == null || txtPassword.getText().length() == 0) {
            errorMessage += "Le champ Password est vide!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
        	alertError("Enregistrement", "Erreur", errorMessage);
            return false;
        }
    }

	public boolean isOkClicked() {
		 return okClicked;
	 }

	 /**
	  * Called when the user clicks cancel.
	  */
	 @FXML
	 private void handleCancel() {
	 	dialogStage.close();
	 }

	 private void alertError(String title, String headerText, String message){
		 Alert alert = new Alert(AlertType.ERROR);
         alert.initOwner(dialogStage);
         alert.setTitle(title);
         alert.setHeaderText(headerText);
         alert.setContentText(message);

         alert.showAndWait();
	 }

	/**
	  * Sets the stage of this dialog.
	  *
	  * @param dialogStage
	  */
	 public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	 }

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}


}
