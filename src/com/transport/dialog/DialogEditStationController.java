package com.transport.dialog;

import java.util.ArrayList;
import java.util.List;

import com.transport.Main;
import com.transport.model.Detail;
import com.transport.model.Network;
import com.transport.model.Station;
import com.transport.view.NetworkConverter;
import com.transport.view.RequestApi;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DialogEditStationController {

	@FXML
	private ComboBox<Network> cbNetwork;
	@FXML
	private TextField txtStationName;
	@FXML
	private TextField txtStationRank;
	@FXML
	private DialogPane dpStation;
	@FXML
	private Button btAction;

	private Network networkTemp;
	private Station stationTemp;

	private Stage dialogStage;
	private boolean okClicked = false;
	private Main main;

	@FXML
	private void initialize() {
		cbNetwork.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Network>()
		{
			public void changed(ObservableValue<? extends Network> ov,
					final Network oldValue, final Network newValue)
			{
				System.out.println("New selection : "+newValue+" from :"+oldValue);
				networkTemp = newValue;
				refreshRank();
			}
		});
	}
	/**
	 *
	 * @param network
	 */
	 public void setFields(Station station) {
		 stationTemp = station;
		 cbNetwork.setConverter(new NetworkConverter());
		 if(station == null){
			 dpStation.setHeaderText("Ajouter le nom de la station");
			 cbNetwork.getItems().addAll(main.getNetworkData());
			 btAction.setText("Ajouter");
		 }else{
			 RequestApi.convertFromStationToJson(station, "api/station", 1);
			 Network network = RequestApi.convertFromJsonToANetwork();
			 this.networkTemp = network;
			 System.out.println(this.networkTemp.getName());
			 List<Network> networks = new ArrayList<Network>();
			 networks.add(network);
			 main.getStation().setNetworks(networks);
			 dpStation.setHeaderText("Modifier le nom de la station");
			 cbNetwork.getItems().addAll(main.getNetworkData());
			 cbNetwork.getSelectionModel().select(network);
			 btAction.setText("Modifier");
			 txtStationName.setText(station.getName().getValue());
			 txtStationRank.setText(station.getRank().getValue().toString());
		 }
	 }

	 @FXML
	 public void refreshRank(){
		 int maxRank = 0;
		 for (Station s : main.getStationData()) {
			 if(s.getNetworks().get(0).getId().intValue() == networkTemp.getId().intValue()){
				 if(s.getRank().intValue() > maxRank){
					 maxRank = s.getRank().getValue().intValue();
				 }
			}
		 }
		 txtStationRank.setText(String.valueOf(maxRank+1));
		 refreshRankStation(maxRank+1);
	 }

	 private void refreshRankStation(int rank){

		 for (Station s : main.getStationData()) {
			 if(s.getRank().intValue() == rank){
				 rank++;
				 s.setRank(rank);
				 RequestApi.convertFromStationToJson(s, "api/station/update", 2);
			}
		 }

	 }

	@FXML
	public void action(){
		if(isInputValid()){
			if(main.getStation() == null){
				Station station = new Station(txtStationName.getText(), Integer.valueOf(txtStationRank.getText()));

				int maxSize = 0;
				for (Station s : main.getStationData()) {
					if(s.getId().getValue().intValue() > maxSize){
						maxSize = s.getId().getValue().intValue();
					}
				}
				station.setId(maxSize+1);
				station.setBiFlux(false);
				List<Network> networks = new ArrayList<Network>();
				networks.add(cbNetwork.selectionModelProperty().getValue().getSelectedItem());
				station.setNetworks(networks);
				List<Detail> d = new ArrayList<Detail>();
				station.setDetails(d);
				RequestApi.convertFromStationToJson(station, "api/station/create", 1);

				refresh();
			}else{
				main.setNetwork(cbNetwork.selectionModelProperty().getValue().getSelectedItem());
				main.getStation().setName(txtStationName.getText());
				RequestApi.convertFromStationToJson(main.getStation(), "api/station/update", 2);

				refresh();
			}
		}
		main.setNetwork(null);
		okClicked = true;
		dialogStage.close();
	}

	 private void refresh(){
		main.showOverview();

		/*RequestApi.getAll("api/stations", Master.getToken());
		main.getStationData().clear();
		main.getStationData().addAll(RequestApi.convertFromJsonToStation());

		RequestApi.getAll("api/details", Master.getToken());
		main.getDetailData().clear();
		main.getDetailData().addAll(RequestApi.convertFromJsonToDetail());*/
	}

	public boolean isOkClicked() {
		 return okClicked;
	 }

	 /**
	  * Called when the user clicks cancel.
	  */
	 @FXML
	 private void handleCancel() {
	 	dialogStage.close();
	 }

	 private boolean isInputValid() {
	        String errorMessage = "";

	        if (txtStationName.getText() == null || txtStationName.getText().length() == 0) {
	            errorMessage += "Le champ Nom est vide!\n";
	        }

	        if (txtStationRank.getText() == null || txtStationRank.getText().length() == 0) {
	            errorMessage += "Le champ Ordre est vide!\n";
	        }

	        if (cbNetwork.selectionModelProperty() == null) {
	            errorMessage += "Le champ Réseau est vide!\n";
	        }

	        if (errorMessage.length() == 0) {
	            return true;
	        } else {
	        	alertError("Enregistrement", "Erreur", errorMessage);
	            return false;
	        }
	    }


	 private void alertError(String title, String headerText, String message){
		 Alert alert = new Alert(AlertType.ERROR);
         alert.initOwner(dialogStage);
         alert.setTitle(title);
         alert.setHeaderText(headerText);
         alert.setContentText(message);

         alert.showAndWait();
	 }
	/**
	  * Sets the stage of this dialog.
	  *
	  * @param dialogStage
	  */
	 public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	 }

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

}
