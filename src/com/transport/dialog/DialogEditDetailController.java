package com.transport.dialog;

import com.transport.Main;
import com.transport.model.Detail;
import com.transport.model.Station;
import com.transport.view.Master;
import com.transport.view.RequestApi;
import com.transport.view.StationConverter;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.Util;

public class DialogEditDetailController {
	@FXML
	private ComboBox<Station> cbStationA;
	@FXML
	private ComboBox<Station> cbStationB;
	@FXML
	private TextField txtTime;
	@FXML
	private DialogPane dpDetail;

	@FXML
	private Button btAction;

	private Stage dialogStage;
	private boolean okClicked = false;
	private Main main;

	/**
	 *
	 * @param network
	 */
	 public void setFields(Detail detail) {
		 txtTime.textProperty().addListener(new ChangeListener<String>() {
		        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		            if (!newValue.matches("\\d*")) {
		            	txtTime.setText(newValue.replaceAll("[^\\d]", ""));
		            }
		        }
		    });

		 cbStationA.setConverter(new StationConverter());
		 cbStationB.setConverter(new StationConverter());
		 if(detail == null){
			 dpDetail.setHeaderText("Ajouter des détails");
			 btAction.setText("Ajouter");
			 cbStationA.getItems().addAll(main.getStationData());
			 cbStationB.getItems().addAll(main.getStationData());
		 }else{
			 dpDetail.setHeaderText("Modifier les détails");
			 btAction.setText("Modifier");
			 cbStationA.getItems().addAll(main.getStationData());
			 cbStationA.getSelectionModel().select(detail.getIdStationA().get());
			 cbStationB.getItems().addAll(main.getStationData());
			 cbStationB.getSelectionModel().select(detail.getIdStationB().get());
			 txtTime.setText(detail.getTime().getValue().toString());
		 }
	 }

	@FXML
	public void action(){
		if(isInputValid()){
			if(main.getDetail() == null){
				Detail detail = new Detail(cbStationA.selectionModelProperty().getValue().getSelectedItem(),
						cbStationB.selectionModelProperty().getValue().getSelectedItem(),
						Integer.valueOf(txtTime.getText()));

				int maxSize = 0;
				for (Detail d : main.getDetailData()) {
					if(d.getId().getValue().intValue() > maxSize){
						maxSize = d.getId().getValue().intValue();
					}
				}
				detail.setId(maxSize+1);
				RequestApi.convertFromDetailToJson(detail, "api/detail/create", 1);

				refresh();
			}else{
				main.getDetail().setIdStationA(cbStationA.selectionModelProperty().getValue().getSelectedItem());
				main.getDetail().setIdStationB(cbStationB.selectionModelProperty().getValue().getSelectedItem());
				main.getDetail().setTime(Integer.valueOf(txtTime.getText()));
				RequestApi.convertFromDetailToJson(main.getDetail(), "api/detail/update", 2);

				refresh();
			}
		}
		main.setNetwork(null);
		okClicked = true;
		dialogStage.close();
	}

	 private void refresh(){
		RequestApi.getAll("api/details", Master.getToken());
		main.getDetailData().clear();
		main.getDetailData().addAll(RequestApi.convertFromJsonToDetail());
	}

	 private boolean isInputValid() {
        String errorMessage = "";

        if (cbStationA.selectionModelProperty() == null) {
            errorMessage += "Le champ Station A est vide!\n";
        }

        if (cbStationB.selectionModelProperty() == null) {
            errorMessage += "Le champ Station A est vide!\n";
        }

        if (txtTime.getText() == null || txtTime.getText().length() == 0) {
            errorMessage += "Le champ Délai n'est pas correct !\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
        	Util.alertError(dialogStage, "Enregistrement", "Erreur", errorMessage);
            return false;
        }
    }

	public boolean isOkClicked() {
		 return okClicked;
	}

	 /**
	  * Called when the user clicks cancel.
	  */
	 @FXML
	 private void handleCancel() {
	 	dialogStage.close();
	 }

	/**
	  * Sets the stage of this dialog.
	  *
	  * @param dialogStage
	  */
	 public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	 }

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

}
