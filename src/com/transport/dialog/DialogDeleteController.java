package com.transport.dialog;

import com.transport.Main;
import com.transport.view.Master;
import com.transport.view.RequestApi;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import util.Util;

public class DialogDeleteController {
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnCancel;
	@FXML
	private Label lblComponent;

	private Stage dialogStage;
	private boolean okClicked = false;

	private Main main;

	@FXML
	public void delete(){
		if(main.getNetwork() != null){
			deleteNetwork();
		}
		if(main.getStation() != null){
			deleteStation();
		}

		if(main.getDetail() != null){
			deleteDetail();
		}

		okClicked = true;
		dialogStage.close();
	}

	private void deleteDetail(){
		RequestApi.convertFromDetailToJson(main.getDetail(), "api/detail/delete", 3);
		main.setDetail(null);
		checkRemove(3);
	}

	private void deleteStation(){
		RequestApi.convertFromStationToJson(main.getStation(), "api/station/delete", 3);
		main.setStation(null);
		checkRemove(2);
	}

	private void deleteNetwork(){
		RequestApi.convertFromNetworkToJson(main.getNetwork(), "api/network/delete", 3);
		main.setNetwork(null);
		checkRemove(1);
	}

	private void checkRemove(int type){
		if(!RequestApi.convertFromJsonToBoolean()){
			Util.alertError(dialogStage, "Erreur", "Problème de suppression", "Veuillez vérifier votre connexion.");
		}else{
			refresh(type);
		}
	}

	private void refresh(int type){
		switch (type) {
		case 1:
			refreshNetworks();
			refreshStations();
			refreshDetails();
			break;
		case 2:
			refreshStations();
			refreshDetails();
			break;
		case 3:
			refreshDetails();
			break;
		default:
			refreshNetworks();
			refreshStations();
			refreshDetails();
			break;
		}

	}

	private void refreshNetworks(){
		RequestApi.getAll("api/networks", Master.getToken());
		main.getNetworkData().clear();
		main.getNetworkData().addAll(RequestApi.convertFromJsonToNetwork());
	}

	private void refreshStations(){
		/*RequestApi.getAll("api/stations", Master.getToken());
		main.getStationData().clear();
		main.getStationData().addAll(RequestApi.convertFromJsonToStation());*/
		main.showOverview();

		//OverviewController.cbStart.getItems().addAll(main.getStationData());
		//OverviewController.cbEnd.getItems().addAll(main.getStationData());
	}

	private void refreshDetails(){
		RequestApi.getAll("api/details", Master.getToken());
		main.getDetailData().clear();
		main.getDetailData().addAll(RequestApi.convertFromJsonToDetail());
	}

	public boolean isOkClicked() {
		return okClicked;
	}

	public void setOkClicked(boolean okClicked) {
		this.okClicked = okClicked;
	}

	/**
	  * Called when the user clicks cancel.
	  */
	 @FXML
	 private void handleCancel() {
	 	dialogStage.close();
	 }

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

}
