package com.transport.dialog;


import com.transport.Main;
import com.transport.model.Network;
import com.transport.view.Master;
import com.transport.view.RequestApi;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.Util;

public class DialogEditNetworkController {
	@FXML
	private TextField txtNetwork;
	@FXML
	private DialogPane dpNetwork;

	@FXML
	private Button btAction;

	private Stage dialogStage;
	private boolean okClicked = false;

	private Main main;

	/**
	 *
	 * @param network
	 */
	 public void setFields(Network network) {
		 if(network == null){
			 dpNetwork.setHeaderText("Ajouter le nom du réseau");
			 btAction.setText("Ajouter");
		 }else{
			 dpNetwork.setHeaderText("Modifier le nom du réseau");
			 btAction.setText("Modifier");
			 txtNetwork.setText(network.getName().getValue());
		 }
	 }

	@FXML
	public void action(){
		if(isInputValid()){
			if(main.getNetwork() == null){
				Network network = new Network(txtNetwork.getText());
				RequestApi.getAll("api/network/last", Master.getToken());

				int maxSize = 0;
				for (Network n : main.getNetworkData()) {
					if(n.getId().getValue().intValue() > maxSize){
						maxSize = n.getId().getValue().intValue();
					}
				}
				network.setId(maxSize+1);
				//network.setAuser(auser);
				RequestApi.convertFromNetworkToJson(network, "api/network/create", 1);

				refresh();
				/*
				 * Ajouter l'envoi vers l'api
				 * Retourne la nouvelle liste de réseau.
				 */
			}else{
				main.getNetwork().setName(txtNetwork.getText());
				RequestApi.convertFromNetworkToJson(main.getNetwork(), "api/network/update", 2);

				refresh();
			}
		}
		main.setNetwork(null);
		okClicked = true;
		dialogStage.close();
	}

	private void refresh(){
		RequestApi.getAll("api/networks", Master.getToken());
		main.getNetworkData().clear();
		main.getNetworkData().addAll(RequestApi.convertFromJsonToNetwork());
	}

	private boolean isInputValid() {
        String errorMessage = "";

        if (txtNetwork.getText() == null || txtNetwork.getText().length() == 0) {
            errorMessage += "Le champ est vide!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
        	Util.alertError(dialogStage, "Enregistrement", "Erreur", errorMessage);
            return false;
        }
    }

	public boolean isOkClicked() {
		 return okClicked;
	 }

	 /**
	  * Called when the user clicks cancel.
	  */
	 @FXML
	 private void handleCancel() {
	 	dialogStage.close();
	 }

	/**
	  * Sets the stage of this dialog.
	  *
	  * @param dialogStage
	  */
	 public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	 }

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}


}
