package com.transport.dialog;


import com.transport.Main;
import com.transport.model.StationForItinerary;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class DialogItineraryController {
	@FXML
	private TextArea txtareaItinerary;

	private Stage dialogStage;
	private boolean okClicked = false;

	private Main main;

    public void loadData() {
    	StringBuilder value = new StringBuilder();
		int nbStation = 0;
		for (StationForItinerary station : main.getItinerary().getStationForItinerary()) {
			if(nbStation == 0){
				value.append("n° "+nbStation+" : "+station.getName()+"\n");
			}else{
				value.append("n° "+nbStation+" : "+station.getName()+" - "+station.getDetail().getTime().intValue()+"\n");
			}
			nbStation++;
		}
		value.append("Total : "+main.getItinerary().getTimeCourse());
		txtareaItinerary.setText(value.toString());
    }

	public boolean isOkClicked() {
		 return okClicked;
	 }

	 /**
	  * Called when the user clicks cancel.
	  */
	 @FXML
	 private void handleCancel() {
	 	dialogStage.close();
	 }

	/**
	  * Sets the stage of this dialog.
	  *
	  * @param dialogStage
	  */
	 public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	 }

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

}
