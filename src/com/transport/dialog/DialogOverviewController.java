package com.transport.dialog;

import com.transport.model.Network;
import com.transport.model.Station;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DialogOverviewController {
	@FXML
	private Label lblWhat;
	@FXML
	private RadioButton rdNetwork;
	@FXML
	private RadioButton rdStation;
	@FXML
	private RadioButton rdDetail;
	@FXML
	private TextField txtTime;
	@FXML
	private ComboBox<Network> networks;
	@FXML
	private ComboBox<Station> stationsA;
	@FXML
	private ComboBox<Station> stationsB;
	// private ObservableList<Categorie> categorieData = FXCollections.observableArrayList();
	private Stage dialogStage;
	private boolean okClicked = false;/*
	private Categorie cate;

	public ObservableList<Categorie> getCategorieData() {
        return categorieData;
    }*/

	 /**
	  * Sets the stage of this dialog.
	  *
	  * @param dialogStage
	  */
	 public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	 }

	 public void setFields(Network network) {
		// categorieData.addAll(Connection.getAllCategory());
	    // categorieData.addAll(tempCategorie);
		 lblWhat.setText("Mise à jour d'un réseau.");
		 rdNetwork.setSelected(true);
		 //networks.getItems().addAll(arg0);
	     txtTime.setText("Choisir");
	 }

	 /**
	     * Called when the user clicks ok.
	     */
	   /* @FXML
	    private void handleOk() {

	        if (isInputValid()) {
	        	System.out.println(categorieField.getSelectionModel().getSelectedItem().getId()+ " "+categorieField.getSelectionModel().getSelectedItem().getNom());
	          //  cate.setId(categorieField.getSelectionModel().getSelectedItem().getId());
	           // cate.setNom(categorieField.getSelectionModel().getSelectedItem().getNom());
	           // cate.setProduit(categorieField.getSelectionModel().getSelectedItem().getProduit());
	            cate=categorieField.getSelectionModel().getSelectedItem();
	            cate.setNom(nomField.getText());
	            Categorie aCategory = Connection.updateCategory(cate);

	            if(Integer.valueOf(aCategory.getId())==null){
	            	Alert alert = new Alert(AlertType.ERROR);
		            alert.initOwner(dialogStage);
		            alert.setTitle("Enregistrement");
		            alert.setHeaderText("Erreur");
		            alert.setContentText("Un problème est survenu pendant l'enregistrement.");

		            alert.showAndWait();
	            }else{
	            	Alert alert = new Alert(AlertType.INFORMATION);
	            	alert.setTitle("Information");
	            	alert.setHeaderText("Changement");
	            	alert.setContentText("Le changement va être pris en compte après le redémarrage de l'application.");

	            	alert.showAndWait();

	            }
	            okClicked = true;
	            dialogStage.close();
	        }
	    }*/

	 public boolean isOkClicked() {
		 return okClicked;
	 }

	 /**
	  * Called when the user clicks cancel.
	  */
	 @FXML
	 private void handleCancel() {
	 	dialogStage.close();
	 }

	/* private boolean isInputValid() {
	        String errorMessage = "";

	        if (nomField.getText() == null || nomField.getText().length() == 0) {
	            errorMessage += "No valid code!\n";
	        }

	        if (categorieField.selectionModelProperty().getValue().getSelectedItem() == null){
	            errorMessage += "No valid Categorie Field!\n";
	        }

	        if (errorMessage.length() == 0) {
	            return true;
	        } else {
	            // Show the error message.
	            Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(dialogStage);
	            alert.setTitle("Invalid Fields");
	            alert.setHeaderText("Please correct invalid fields");
	            alert.setContentText(errorMessage);

	            alert.showAndWait();

	            return false;
	        }
	    }*/
}
