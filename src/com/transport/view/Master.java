package com.transport.view;

import com.transport.model.Token;

public class Master {
	private static Token token;

	public static Token getToken() {
		return token;
	}

	public static void setToken(Token token) {
		Master.token = token;
	}


}
