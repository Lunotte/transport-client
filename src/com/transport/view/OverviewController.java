package com.transport.view;

import com.transport.Main;
import com.transport.model.Detail;
import com.transport.model.Itinerary;
import com.transport.model.Network;
import com.transport.model.Station;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;


public class OverviewController{

	//Part Solveur
	@FXML
	private ComboBox<Station> cbStart;
	@FXML
	private ComboBox<Station> cbEnd;
	@FXML
	private Button btSolveur;

	//Part Table Station
	@FXML
	private TableView<Station> stationTable;
	@FXML
	private TableColumn<Station, String> nameColumn;
	@FXML
	private TableColumn<Station, Integer> rankColumn;

	//Part Table Detail
	@FXML
	private TableView<Detail> detailTable;
	@FXML
	private TableColumn<Detail, String> stationAColumn;
	@FXML
	private TableColumn<Detail, String> stationBColumn;
	@FXML
	private TableColumn<Detail, Integer> timeColumn;
	@FXML
	private ListView<Network> lstNetworks;

	private Main main;

	//Part context menu
	private ContextMenu contextMenuNetwork;
	private ContextMenu contextMenuStation;
	private ContextMenu contextMenuDetail;

	/**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize component
    	init();
    }

    public void init(){
    	nameColumn.setCellValueFactory(cellData -> cellData.getValue().getName());
    	rankColumn.setCellValueFactory(cellData -> cellData.getValue().getRank().asObject());

    	stationAColumn.setCellValueFactory(cellDataDetail -> cellDataDetail.getValue().getIdStationA().getValue().getName());
    	stationBColumn.setCellValueFactory(cellDataDetail -> cellDataDetail.getValue().getIdStationB().getValue().getName());
    	timeColumn.setCellValueFactory(cellDataDetail -> cellDataDetail.getValue().getTime().asObject());

    	lstNetworks.setCellFactory(new NetworkCellFactory());
    	cbStart.setConverter(new StationConverter());
    	cbEnd.setConverter(new StationConverter());

    	initContextMenu();
    }

    public void loadData(){
    	main.getNetworkData().clear();
    	main.getStationData().clear();
    	main.getDetailData().clear();

    	if(main.isStateServer()){
	    	RequestApi.getAll("api/networks", Master.getToken());
			main.getNetworkData().addAll(RequestApi.convertFromJsonToNetwork());

			RequestApi.getAll("api/stations", Master.getToken());
			main.getStationData().addAll(RequestApi.convertFromJsonToStation());

			RequestApi.getAll("api/details", Master.getToken());
			main.getDetailData().addAll(RequestApi.convertFromJsonToDetail());

			cbStart.getItems().addAll(main.getStationData());
			cbEnd.getItems().addAll(main.getStationData());
    	}
    }

    private void initContextMenu(){
    	initContextMenuStation();
    	initContextMenuDetail();
    	initContextMenuNetwork();
    }

    private void initContextMenuStation(){
    	contextMenuStation = new ContextMenu();
        MenuItem create = new MenuItem("Ajouter");
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                main.setStation(null);
                handleEditStation();
            }
        });

        MenuItem update = new MenuItem("Modifier");
        update.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if(stationTable.getSelectionModel().getSelectedItem() instanceof Station){
	            	Station station = stationTable.getSelectionModel().getSelectedItem();
	                main.setStation(station);
	                if (station != null){
	                	System.out.println(station);
	                	handleEditStation();
	                }
            	}
            }
        });

        MenuItem delete = new MenuItem("Supprimer");
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if(stationTable.getSelectionModel().getSelectedItem() instanceof Station){
	                main.setStation(stationTable.getSelectionModel().getSelectedItem());
                	handleDelete();
            	}
            }
        });

        contextMenuStation.getItems().addAll(create, update, delete);
        stationTable.setContextMenu(contextMenuStation);
    }


    private void initContextMenuDetail(){
    	contextMenuDetail = new ContextMenu();
        MenuItem create = new MenuItem("Ajouter");
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
            	main.setDetail(null);
                handleEditDetail();
            }
        });

        MenuItem update = new MenuItem("Modifier");
        update.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if(detailTable.getSelectionModel().getSelectedItem() instanceof Detail){
	            	Detail detail = detailTable.getSelectionModel().getSelectedItem();
	                main.setDetail(detail);
	                if (detail != null){
	                	System.out.println(detail);
	                	handleEditDetail();
	                }
            	}
            }
        });

        MenuItem delete = new MenuItem("Supprimer");
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if(detailTable.getSelectionModel().getSelectedItem() instanceof Detail){
	                main.setDetail(detailTable.getSelectionModel().getSelectedItem());
                	handleDelete();
            	}
            }
        });

        // Add MenuItem to ContextMenu
        contextMenuDetail.getItems().addAll(create, update, delete);
        detailTable.setContextMenu(contextMenuDetail);
    }
    private void initContextMenuNetwork(){
    	contextMenuNetwork = new ContextMenu();
        MenuItem create = new MenuItem("Ajouter");
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
            	main.setNetwork(null);
                handleEditNetwork();
            }
        });

        MenuItem update = new MenuItem("Modifier");
        update.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if(lstNetworks.getSelectionModel().getSelectedItem() instanceof Network){
	                Network network = lstNetworks.getSelectionModel().getSelectedItem();
	                main.setNetwork(network);
	                if (network != null){
	                	System.out.println(network);
	                	handleEditNetwork();
	                }
            	}
            }
        });

        MenuItem delete = new MenuItem("Supprimer");
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if(lstNetworks.getSelectionModel().getSelectedItem() instanceof Network){
	                main.setNetwork(lstNetworks.getSelectionModel().getSelectedItem());
                	handleDelete();
            	}
            }
        });

        // Add MenuItem to ContextMenu
        contextMenuNetwork.getItems().addAll(update, delete);
        lstNetworks.setContextMenu(contextMenuNetwork);
    }


    @FXML
    public void clickItemStation(MouseEvent event)
    {
    	if (event.getClickCount() == 2){
    		main.setStation(stationTable.getSelectionModel().getSelectedItem());
    		handleEditStation();
    	}
    }

    @FXML
    public void clickItemNetwork(MouseEvent event)
    {
    	if (event.getClickCount() == 2){
    		main.setNetwork(lstNetworks.getSelectionModel().getSelectedItem());
    		handleEditNetwork();
    	}
    }

    @FXML
    public void clickItemDetail(MouseEvent event)
    {
    	if (event.getClickCount() == 2){
    		main.setDetail(detailTable.getSelectionModel().getSelectedItem());
    		handleEditDetail();
    	}
    }

    @FXML
    private void handleEditNetwork() {
    	main.showDialogEditNetwork();
    }

    @FXML
    private void handleEditStation() {
        main.showDialogEditStation();
    }

    @FXML
    private void handleEditDetail() {
        main.showDialogEditDetail();
    }

    @FXML
    private void handleDelete() {
    	main.showDialogDelete();
    }

    @FXML
    private void handleShowItinerary() {
    	main.showDialogItinerary();
    }

    @FXML
    public void actionSolveur(){
    	if(cbStart.selectionModelProperty().getValue().getSelectedItem() != null &&
    			cbEnd.selectionModelProperty().getValue().getSelectedItem() != null){
	    	System.out.println("Solveur en cours ...");
	    	//System.out.println(cbStart.selectionModelProperty().getValue().getSelectedItem());
	    	//System.out.println(cbEnd.selectionModelProperty().getValue().getSelectedItem());
	    	RequestApi.getAll("api/solveur/id/"+cbStart.selectionModelProperty().getValue().getSelectedItem().getId().intValue()+"/"+
	    			cbEnd.selectionModelProperty().getValue().getSelectedItem().getId().intValue(), Master.getToken());
	    	Itinerary itinerary = RequestApi.convertFromJsonToOneItinerary();
			main.setItinerary(itinerary);
	    	handleShowItinerary();
    	}
    }

	public void setMain(Main main) {
		this.main = main;
		stationTable.setItems(main.getStationData());
		detailTable.setItems(main.getDetailData());
		lstNetworks.setItems(main.getNetworkData());
    }

	public Main getMain() {
		return main;
	}

}
