package com.transport.view;

import com.transport.model.Network;

import javafx.scene.control.ListCell;

public class NetworkCell extends ListCell<Network>{
	@Override
	public void updateItem(Network item, boolean empty)
	{
		super.updateItem(item, empty);

		int index = this.getIndex();
		String name = null;

		// Format name
		if (item == null || empty)
		{
		}
		else
		{
			name = (index + 1) + ". " +
			item.getName().getValue();
		}

		this.setText(name);
		setGraphic(null);
	}
}
