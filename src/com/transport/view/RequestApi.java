package com.transport.view;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.hildan.fxgson.FxGson;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.transport.model.Detail;
import com.transport.model.Itinerary;
import com.transport.model.Network;
import com.transport.model.Station;
import com.transport.model.Token;
import com.transport.model.User;

public class RequestApi {

	static HttpClient httpClient = HttpClientBuilder.create().build();
    static String url = "http://localhost:8080/";// put in your url
    static Gson gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    static HttpPost post;
    static HttpGet get;
    static HttpPut put;
    static HttpDelete delete;
    static StringEntity postingString = null;
	static String responseString;
	static Token token;
	static ArrayList<NameValuePair> postParameters;

	private static void finish(){
		url = "http://localhost:8080/";
	}

	public static boolean get(String postUrl){
    	url += postUrl;
    	post = new HttpPost(url);
    	finish();
        try {
    		HttpResponse  response = httpClient.execute(post);
    		if(response.getStatusLine().getStatusCode() == 200){
    			return true;
    		}else{
    			return false;
    		}
    	} catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		return false;
    	} catch (ConnectException e) {
    		// TODO Auto-generated catch block
    		//e.printStackTrace();
    		return false;
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		return false;
    	}
    }

    private static void getData(){
    	//gson.tojson() converts your pojo to json
       // post.setEntity(postingString);
        get.setHeader("Content-type", "application/json");
        if(token != null){
        	get.setHeader("X-Authorization","Bearer "+token.getToken());
        }
        try {
    		HttpResponse  response = httpClient.execute(get);
    		HttpEntity entity = response.getEntity();
    		responseString = EntityUtils.toString(entity, "UTF-8");
    		//System.out.println(responseString);
    		gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    		//System.out.println(on.fromJson(responseString, Network.class));
    	} catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }

    private static void postData(){
    	//gson.tojson() converts your pojo to json
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        if(token != null){
        	post.setHeader("X-Authorization","Bearer "+token.getToken());
        }
        try {
    		HttpResponse  response = httpClient.execute(post);
    		HttpEntity entity = response.getEntity();
    		responseString = EntityUtils.toString(entity, "UTF-8");
    		//System.out.println(responseString);
    		gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    		//System.out.println(on.fromJson(responseString, Network.class));
    	} catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }

    private static void putData(){
    	//gson.tojson() converts your pojo to json
    	put.setEntity(postingString);
    	put.setHeader("Content-type", "application/json");
    	put.setHeader("X-Authorization","Bearer "+token.getToken());
        try {
    		HttpResponse  response = httpClient.execute(put);
    		HttpEntity entity = response.getEntity();
    		responseString = EntityUtils.toString(entity, "UTF-8");
    		//System.out.println(responseString);
    		gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    		//System.out.println(on.fromJson(responseString, Network.class));
    	} catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }

    private static void deleteData(){
    	//gson.tojson() converts your pojo to json
    	//delete.setEntity(postingString);


    	/*
    	 * L'id est dans l'url
    	 */

    	delete.setHeader("Content-type", "application/json");
    	delete.setHeader("X-Authorization","Bearer "+token.getToken());
        try {
    		HttpResponse  response = httpClient.execute(delete);
    		HttpEntity entity = response.getEntity();
    		responseString = EntityUtils.toString(entity, "UTF-8");
    		//System.out.println(responseString);
    		gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    		//System.out.println(on.fromJson(responseString, Network.class));
    	} catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }

    public static void getAll(String getUrl, Token atoken){
    	token = atoken;
    	url += getUrl;
    	get = new HttpGet(url);
    	finish();
    	getData();
    }

    /**
     *
     * @param user
     * @param postUrl
     * @param typeRequest -> 1: POST - 2: PUT - 3: DELETE
     */
    public static void convertFromUserToJson(User user, String postUrl, int typeRequest){
    	if(typeRequest == 1 || typeRequest == 2){
    		try {
	    		postingString = new StringEntity(gson.toJson(user));
	    	} catch (UnsupportedEncodingException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
    		loadRequest(postUrl, typeRequest, true, null);
    	}else{
    		loadRequest(postUrl, typeRequest, true, user.getId());
    	}
    }

    /**
     *
     * @param network
     * @param postUrl
     * @param typeRequest -> 1: POST - 2: PUT - 3: DELETE
     */
    public static void convertFromNetworkToJson(Network network, String postUrl, int typeRequest){
    	if(typeRequest == 1 || typeRequest == 2){
    		try {
	    		postingString = new StringEntity(gson.toJson(network));
	    	} catch (UnsupportedEncodingException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
    		loadRequest(postUrl, typeRequest, true, null);
    	}else{
    		loadRequest(postUrl, typeRequest, true, network.getId().getValue().longValue());
    	}
    }

    /**
     *
     * @param station
     * @param postUrl
     * @param typeRequest -> 1: POST - 2: PUT - 3: DELETE
     */
    public static void convertFromStationToJson(Station station, String postUrl, int typeRequest){
	    if(typeRequest == 1 || typeRequest == 2){
    		try {
	    		postingString = new StringEntity(gson.toJson(station));
	    	} catch (UnsupportedEncodingException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
    		loadRequest(postUrl, typeRequest, true, null);
		}else{
			loadRequest(postUrl, typeRequest, true, station.getId().getValue().longValue());
		}
    }


   public static void convertFromStationTwoToJson(HashMap<String,Station> stations, String postUrl){
	   postParameters = new ArrayList<NameValuePair>();
   		try {
   			for(Map.Entry<String, Station> entry : stations.entrySet()) {
   			    postParameters.add(new BasicNameValuePair(entry.getKey(), gson.toJson(entry.getValue())));
   			}
   			postingString = new UrlEncodedFormEntity(postParameters);
    	} catch (UnsupportedEncodingException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
   		url += postUrl;
    	post = new HttpPost(url);
    	finish();
   		postData();
   }


    /**
    *
    * @param detail
    * @param postUrl
    * @param typeRequest -> 1: POST - 2: PUT - 3: DELETE
    */
   public static void convertFromDetailToJson(Detail detail, String postUrl, int typeRequest){
	    if(typeRequest == 1 || typeRequest == 2){
   		try {
	    		postingString = new StringEntity(gson.toJson(detail));
	    	} catch (UnsupportedEncodingException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
   		loadRequest(postUrl, typeRequest, true, null);
		}else{
			loadRequest(postUrl, typeRequest, true, detail.getId().getValue().longValue());
		}
   }

    public static boolean convertFromJsonToBoolean(){
    	Type listType = new TypeToken<Boolean>(){}.getType();
    	boolean n = gson.fromJson(responseString, listType);
		return n;
    }

    public static Token convertFromJsonToToken(){
    	Type listType = new TypeToken<Token>(){}.getType();
    	Token n = gson.fromJson(responseString, listType);
		return n;
    }

    public static List<Network> convertFromJsonToNetwork(){
    	Type listType = new TypeToken<ArrayList<Network>>(){}.getType();
		List<Network> n = gson.fromJson(responseString, listType);
		return n;
    }

    public static Network convertFromJsonToANetwork(){
    	Type listType = new TypeToken<Network>(){}.getType();
		Network n = gson.fromJson(responseString, listType);
		return n;
    }

    public static List<Station> convertFromJsonToStation(){
    	Type listType = new TypeToken<ArrayList<Station>>(){}.getType();
		List<Station> n = gson.fromJson(responseString, listType);
		//Network n = gson.fromJson(responseString, Network.class);
		return n;
    }

    public static List<Detail> convertFromJsonToDetail(){
    	Type listType = new TypeToken<ArrayList<Detail>>(){}.getType();
		List<Detail> n = gson.fromJson(responseString, listType);
		return n;
    }

    public static Itinerary convertFromJsonToOneItinerary(){
    	Type listType = new TypeToken<Itinerary>(){}.getType();
		Itinerary n = gson.fromJson(responseString, listType);
		return n;
    }

    private static void loadRequest(String postUrl, int typeRequest, boolean login, Long id){
    	switch (typeRequest) {
		case 1:
			url += postUrl;
	    	post = new HttpPost(url);
	    	finish();
	    	if(login){
	    		post.setHeader("X-Requested-With", "XMLHttpRequest");
    	        post.setHeader("Cache-Control", "no-cache");
	    	}
	    	postData();
			break;
		case 2:
			url += postUrl;
	    	put = new HttpPut(url);
	    	finish();
	    	putData();
			break;
		case 3:
			url += postUrl+"/"+id;
	    	delete = new HttpDelete(url);
	    	finish();
	    	deleteData();
			break;

		default:
			break;
		}
    }
}
