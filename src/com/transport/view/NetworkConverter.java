package com.transport.view;

import java.util.HashMap;
import java.util.Map;

import com.transport.model.Network;

import javafx.util.StringConverter;

public class NetworkConverter extends StringConverter<Network>{

	/** Cache de productos */
	private Map<String, Network> mapNetwork = new HashMap<String, Network>();

	@Override
	public String toString(Network network) {
		mapNetwork.put(network.getId().getValue().toString(), network);
	    return network.getName().getValue();
	}

	@Override
	public Network fromString(String codigo) {
	    return mapNetwork.get(codigo);
	}

}
