package com.transport.view;

import com.transport.model.Network;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class NetworkCellFactory implements Callback<ListView<Network>, ListCell<Network>>{
	@Override
	public ListCell<Network> call(ListView<Network> listview)
	{
		return new NetworkCell();
	}
}
