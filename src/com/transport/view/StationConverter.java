package com.transport.view;

import java.util.HashMap;
import java.util.Map;

import com.transport.model.Station;

import javafx.util.StringConverter;

public class StationConverter extends StringConverter<Station>{

	/** Cache de productos */
	private Map<String, Station> mapStation = new HashMap<String, Station>();

	@Override
	public String toString(Station station) {
		mapStation.put(station.getId().getValue().toString(), station);
	    return station.getName().getValue();
	}

	@Override
	public Station fromString(String codigo) {
	    return mapStation.get(codigo);
	}

}
