package com.transport.view;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import com.transport.Main;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import util.Writer;


/**
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar and space where other JavaFX
 * elements can be placed.
 *
 * @author Marco Jakob
 */
public class RootLayoutController {

    // Reference to the main application
    private Main main;
    private Runtime rt1;
	private String pathGlpk;
	@FXML
	private MenuItem login;
	@FXML
	private MenuItem startServer;
	@FXML
	private MenuItem stopServer;
	@FXML
	private Label lblBar;
	@FXML
	private ProgressBar bar;
	private boolean signal = false;

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMain(Main main) {
        this.main = main;
    }

    public Main getMain() {
		return main;
	}
    @FXML
    private void handleOpen(){
    	Stage stageFileChooser = new Stage();
    	DirectoryChooser chooser = new DirectoryChooser();
    	chooser.setTitle("Choisir un répertoire");
    	File selectedDirectory = chooser.showDialog(stageFileChooser);
    	if(selectedDirectory != null){
    		pathGlpk = selectedDirectory.getAbsolutePath();
    		messageAlertInformation(selectedDirectory.getAbsolutePath());

			handleStartServer();
    	}
    }

    /**
     * Handle Start server
     */
    @FXML
    private void handleStartServer() {

    	Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Manipulaion du Serveur");
		alert.setHeaderText("Confirmer la manipulation du serveur");
		alert.setContentText("Souhaitez-vous démarrer le serveur ?\n"+
		"Tant que le serveur n'est pas démarré, aucune action ne sera possible sur le réseau"+
				" de transport.");

		ButtonType buttonTypeYes = new ButtonType("Oui");
		ButtonType buttonTypeNo = new ButtonType("Non");

		alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeYes){
			try {
				Writer.writeBatFile(pathGlpk);
				rt1 = Runtime.getRuntime();
				//Lancement du fichier batch en mode caché
				rt1.exec("cmd /c start /B launcher.bat");
				System.out.println("ouvert");
				main.setStateServer(true);
				startServer.setDisable(true);
			   	lblBar.setVisible(true);
		    	bar.setVisible(true);
		    	bar.setProgress(0.1);
		    	execute();

			} catch (IOException e) {
				e.printStackTrace();
				messageAlertServerError();
			}
		}
    }

    public void execute() {
        Thread main = new Thread(mainRunnable, "main");
        Thread t1 = new Thread(t1Runnable, "t1");
        main.start();
        t1.start();
    }

    public Object myVar = new Object();

    public void finishBar() {
    	Platform.runLater(new Runnable() {
            @Override public void run() {
            	bar.setProgress(1);
            	if(signal){
            		stopServer.setDisable(false);
            		login.setDisable(false);
            		Alert alertInfo = new Alert(AlertType.INFORMATION);
        			alertInfo.setTitle("Succès");
        			alertInfo.setHeaderText(null);
        			alertInfo.setContentText("Le serveur a été démarré avec succès.");

        			alertInfo.showAndWait();

        			main.showDialogConnexion();
            	}else{
            		Alert alertInfo = new Alert(AlertType.INFORMATION);
        			alertInfo.setTitle("Erreur");
        			alertInfo.setHeaderText(null);
        			alertInfo.setContentText("Le serveur n'a pas démarré.");

        			alertInfo.showAndWait();
            	}
            }
        });

    }

    public Runnable t1Runnable = new Runnable() {
        public void run() {
            synchronized(myVar) {
                try {
                	for (int i = 1; i < 10; i++) {
                		bar.setProgress(0.1*i);
                		checkServerStarted();
                		System.out.println("[t1] sleep for 2 sec 5");
                    	Thread.sleep(2500);
                    	if(signal){
                    		i = 10;
                    	}
                    	//System.out.println("[t1] Notifying myVar so Main can invoke myMethod");
                        myVar.notify();
                    }
                } catch (InterruptedException e) {
                    // interupted.
                }
            }
        }
    };

    public Runnable mainRunnable = new Runnable() {
        public void run() {
            synchronized(myVar) {
                try {
                    //System.out.println("[main] Waiting for t1 to notify...");
                    myVar.wait();
                } catch (InterruptedException e) {
                    // interrupted.
                }
                //System.out.println("Call main thread");
                finishBar();
            }
        }
    };

    private void checkServerStarted(){
    	if(RequestApi.get("")){

    		startServer.setDisable(true);
    		//bar.setProgress(1);
    		signal = true;
	   	}

    }

    /**
     * Handle Close server
     */
    @FXML
    private void handleStopServer() {
    	Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Manipulaion du Serveur");
		alert.setHeaderText("Confirmer la manipulation du serveur");
		alert.setContentText("Souhaitez-vous vraiment stopper le serveur ?");

		ButtonType buttonTypeYes = new ButtonType("Oui");
		ButtonType buttonTypeNo = new ButtonType("Non");

		alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeYes){

	        if(RequestApi.get("shutdown")){
			   	main.setStateServer(false);
			   	stopServer.setDisable(true);
			   	startServer.setDisable(false);
			   	login.setDisable(true);
				main.showOverview();

				Alert alertInfo = new Alert(AlertType.INFORMATION);
				alertInfo.setTitle("Succès");
				alertInfo.setHeaderText(null);
				alertInfo.setContentText("Le serveur a été stoppé avec succès.");

				alertInfo.showAndWait();
		   }
		}
    }

	/**
     * Opens an about dialog.
     */
    @FXML
    private void handleConnexion() {
        main.showDialogConnexion();
    }

	/**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
       // alert.setTitle("Gestion");
        alert.setHeaderText("About");
        alert.setContentText("Auteur: Beaugrand Charly");

        alert.showAndWait();
    }

    private void messageAlertServerError(){
    	Alert alert = new Alert(AlertType.WARNING);
    	alert.setTitle("Erreur");
    	alert.setHeaderText("Chemin GLPK");
    	alert.setContentText("Erreur pendant la manipulation du serveur");

    	alert.showAndWait();
    }

    private void messageAlertInformation(String path){
    	Alert alert = new Alert(AlertType.INFORMATION);
    	alert.setTitle("Information");
    	alert.setHeaderText("Chemin GLPK");
    	alert.setContentText("Chemin GLPK ajouté avec succès :\n"+path);

    	alert.showAndWait();
    }
    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }
}