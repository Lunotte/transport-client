package com.transport.model;

public class StationForItinerary {

	private Long id;
	private String name;
	private int rank;
	private Detail detail;

	public StationForItinerary(Long id, String name, int rank, Detail detail) {
		this.id = id;
		this.name = name;
		this.rank = rank;
		this.detail = detail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Detail getDetail() {
		return detail;
	}

	public void setDetail(Detail detail) {
		this.detail = detail;
	}

}
