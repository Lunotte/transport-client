package com.transport.model;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Network {

	private LongProperty id;

	private StringProperty name;

	public LongProperty getId() {
		return id;
	}

	public Network() {
	}

	public Network(String name) {
		this.name = new SimpleStringProperty(name);
	}

	public Network(Long id, String name) {
		this.id = new SimpleLongProperty(id);
		this.name = new SimpleStringProperty(name);
	}

	public void setId(LongProperty id) {
		this.id = id;
	}

	public void setId(int id) {
		this.id = new SimpleLongProperty(id);
	}

	public StringProperty getName() {
		return name;
	}

	public void setName(StringProperty name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = new SimpleStringProperty(name);
	}

}
