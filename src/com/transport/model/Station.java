package com.transport.model;

import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Station {

	private LongProperty id;

	private StringProperty name;

	private IntegerProperty rank;

	private ObservableList<Detail> details;

	private List<Network> networks;

	private BooleanProperty biFlux;

	public Station(Long id, String name, Integer rank, ObservableList<Detail> details, List<Network> networks, Boolean biFlux) {
		this.id = new SimpleLongProperty(id);
		this.name = new SimpleStringProperty(name);
		this.rank = new SimpleIntegerProperty(rank);
		this.details = new SimpleListProperty<Detail>(details);
		this.networks = networks;
		this.biFlux = new SimpleBooleanProperty(biFlux);
	}

	public Station(Long id, String name, Integer rank, List<Network> networks, Boolean biFlux) {
		this.id = new SimpleLongProperty(id);
		this.name = new SimpleStringProperty(name);
		this.rank = new SimpleIntegerProperty(rank);
		this.networks = networks;
		this.biFlux = new SimpleBooleanProperty(biFlux);
	}

	public Station(Long id, String name, Integer rank, List<Network> networks) {
		this.id = new SimpleLongProperty(id);
		this.name = new SimpleStringProperty(name);
		this.rank = new SimpleIntegerProperty(rank);
		this.networks = networks;
	}

	public Station(Long id, String name, Integer rank) {
		this.id = new SimpleLongProperty(id);
		this.name = new SimpleStringProperty(name);
		this.rank = new SimpleIntegerProperty(rank);
	}

	public Station(String name, Integer rank) {
		this.name = new SimpleStringProperty(name);
		this.rank = new SimpleIntegerProperty(rank);
	}

	public LongProperty getId() {
		return id;
	}

	public void setId(LongProperty id) {
		this.id = id;
	}

	public void setId(int id) {
		this.id = new SimpleLongProperty(id);
	}

	public StringProperty getName() {
		return name;
	}

	public void setName(StringProperty name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = new SimpleStringProperty(name);
	}

	public IntegerProperty getRank() {
		return rank;
	}

	public void setRank(IntegerProperty rank) {
		this.rank = rank;
	}

	public void setRank(int i) {
		this.rank = new SimpleIntegerProperty(i);
	}

	public List<Network> getNetworks() {
		return networks;
	}

	public void setNetworks(List<Network> networks) {
		this.networks = networks;
	}


	public BooleanProperty getBiFlux() {
		return biFlux;
	}

	public void setBiFlux(BooleanProperty biFlux) {
		this.biFlux = biFlux;
	}

	public void setBiFlux(Boolean biFlux) {
		this.biFlux = new SimpleBooleanProperty(biFlux);
	}

	public ObservableList<Detail> getDetails() {
		return details;
	}

	public void setDetails(ObservableList<Detail> details) {
		this.details = details;
	}

	public void setDetails(List<Detail> details) {
		this.details = FXCollections.observableList(details);
	}

	@Override
	public String toString() {
		return "Station [id=" + id + ", name=" + name + ", rank=" + rank + "]";
	}

}
