package com.transport.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Detail {

	private LongProperty id;

	private SimpleObjectProperty<Station> idStationA;

	private SimpleObjectProperty<Station> idStationB;

	private IntegerProperty time;

	public Detail() {}

	public Detail(Long id, int time) {
		this.id = new SimpleLongProperty(id);
		this.time = new SimpleIntegerProperty(time);
	}

	public Detail(Station idStationA, Station idStationB, Integer time) {
		this.idStationA = new SimpleObjectProperty<Station>(idStationA);
		this.idStationB = new SimpleObjectProperty<Station>(idStationB);
		this.time = new SimpleIntegerProperty(time);
	}

	public Detail(Long id, Station idStationA, Station idStationB, Integer time) {
		this.id = new SimpleLongProperty(id);
		this.idStationA = new SimpleObjectProperty<Station>(idStationA);
		this.idStationB = new SimpleObjectProperty<Station>(idStationB);
		this.time = new SimpleIntegerProperty(time);
	}

	public SimpleObjectProperty<Station> getIdStationA() {
		return idStationA;
	}

	public void setIdStationA(SimpleObjectProperty<Station> idStationA) {
		this.idStationA = idStationA;
	}

	public void setIdStationA(Station idStationA) {
		this.idStationA = new SimpleObjectProperty<Station>(idStationA);
	}

	public SimpleObjectProperty<Station> getIdStationB() {
		return idStationB;
	}

	public void setIdStationB(SimpleObjectProperty<Station> idStationB) {
		this.idStationB = idStationB;
	}

	public void setIdStationB(Station idStationB) {
		this.idStationB = new SimpleObjectProperty<Station>(idStationB);
	}

	public LongProperty getId() {
		return id;
	}

	public void setId(LongProperty id) {
		this.id = id;
	}

	public void setId(int id) {
		this.id = new SimpleLongProperty(id);
	}

	public IntegerProperty getTime() {
		return time;
	}

	public void setTime(IntegerProperty time) {
		this.time = time;
	}

	public void setTime(Integer time) {
		this.time = new SimpleIntegerProperty(time);
	}

	@Override
	public String toString() {
		return "Detail [id=" + id + ", idStationA=" + idStationA + ", idStationB=" + idStationB + ", time=" + time
				+ "]";
	}

}
