package com.transport.model;

import java.util.List;

public class Itinerary {

	private Long id;

	private List<StationForItinerary> stationForItinerary;

	private int timeCourse;

	public List<StationForItinerary> getStationForItinerary() {
		return stationForItinerary;
	}
	public void setStationForItinerary(List<StationForItinerary> stationForItinerary) {
		this.stationForItinerary = stationForItinerary;
	}
	public int getTimeCourse() {
		return timeCourse;
	}
	public void setTimeCourse(int timeCourse) {
		this.timeCourse = timeCourse;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
