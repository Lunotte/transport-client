package util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Writer {

	public static void writeBatFile(String pathGlpk){
		Charset ENCODING = StandardCharsets.UTF_8;
		String fichierPA = "launcher.bat";

		List<String> line = new ArrayList<String>();
		line.add("@echo off");
		line.add("set PATH=%PATH%;"+pathGlpk);
		line.add("java -jar transport-server.jar");
		line.add("pause");

		try {
			Path path = Paths.get(fichierPA);
			Files.write(path, line, ENCODING);
		} catch (IOException e) {
			System.out.println("Probléme d'écriture du fichier");
		}
	}
}
